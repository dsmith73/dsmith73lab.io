---
layout: post
title:  "- Human Slingshot - "
date:   2005-06-03
excerpt: ""
image: "/images/humanslingshot.jpg"
---


After my X and I were separated, I started getting back into the dating scene. My first date was with this girl named Tammy. We met online and quickly moved to a telephone conversation. We were getting along very well. While we were on the phone she kept talking about how exhillerating it would be to go white-water rafting. Personally, I would love to go white-water rafting! We continued talking. 

After a while, she mentioned that she would like to get together that weekend, but she wasn't certain if I'd like to do what she had in mind. Obviously, the first thing that popped into my mind was white-water rafting! Obviously, I wanted to do it, and I said that I'd love to get together. She persisted that I probably wouldn't want to do what she had in mind. So, to nip it in the bud, I told her that I was committed to doing whatever she had in mind (I already told her that I'd love to go white-water rafting). She insisted that I would decline once I found out what it was. So, I gave her my word. When I say that I'm going to do something, then I'm going to do it, no matter how much I may not want to.

She happily accepted and told me how happy she was that I would go with her and experience the Human Sling-Shot they had out at the Diamond Center Mall... WHAT?!! "Oh shit!," I said. She said that she knew I wouldn't do it. I rebutted, "When I say that I'm going to do something, I do it! That's all there is to it." I could feel the blood draining out of me the whole time. Just thinking about this insane thing had me shaking in fear.

Did I mention that I'm afraid of falling? I don't have a problem being in a high place, so long as I feel like I'm on something sturdy. However, once I'm on an uncertain surface, I can barely contain myself.

Friday rolled around, way too soon in my opinion, and I arrived out at the Diamond Center. She was running a little behind because of construction. As I pulled in, I could feel the blood draining from my body. This thing is supposed to shoot you over 300' into the air, and then you get to fall 300'. There was a man and a woman getting onto this ride. So I decided to see how this contraption would work before it killed me.

I was shaking as I saw them launch, but to my surprise, they weren't screaming. This helped put me at ease. "You can do this," I kept telling myself. "It won't be that bad. It'll be like a rollercoaster ride, and you've done plenty of those." The guy and girl stopped bouncing around and were lowered back to earth. I was breathing much easier and quite a bit more relaxed about this whole thing.

My date still hadn't arrived, and there was a mother daughter duo heading to the ride now. I decided to get a few pictures and some video, so that I could show everyone this crazy thing that I was about to do. So I snapped a couple pictures and began taking some video. As soon as they were launched, a shrill cry eminated from the ball. Their screams made my blood curdle. Immediately I became nauscious, I began shaking uncontrollably, and the realization of my imminent death was flooding my mind. "How could I have let her talk me into this? How could I have been so stupid?"

Tammy came wizzing into the parking lot, and parked across from me. As she got out of her SUV, I saw that she wasn't exactly what I'd hoped she might look like. "Oh well. Just because I'm not going to buy a car, doesn't mean I won't take it for a test drive. And if I'm not interested in a test drive, I'll still look at vehicles and hear the sales pitch." I thought.

We walked up to the counter to get our tickets. The guy selling us the tickets knew Tammy by name. It was like they were old buddies. Come to find out, she was a very frequent flyer at this ride, and I was just her latest "John."

We made our way up to the frame of a ball with a couple little seats strapped to it. We sat down and the ride-guy, let's call him Daryl, began strapping her in. I say Daryl because he reminded me of one of the Daryls on the Bob Newhart show. They were laughing and getting along amazingly well as he securred her seatbelts. I was obviously a third wheel here, with little more meaning to her than my fourty dollars and my ability to fill a second seat on a two person ride.

As Daryl moved over to strap me in, he asked me to remove my shoes. Being the curious person I am, I asked him why I needed to take them off; afterall, I had a bar to put my feet on. "They might fly out into the highway," he said. I forgot to tell you, this ride was about 100' from a highway. Anyway, I immediately responded "How will my shoes fly into the highway? It's at least 100' away from us?" "It's the rules," he said, "You can't have no stuff on you that could fly off." Becomming more nervous, I continued, "Shouldn't my shoes fall right back here? How in the hell are they going to get all the way over there?" Daryl was starting to get impatient with my obvious fear, and said "It's happened before. So, please take your shoes off." I removed my shoes and sat back in the seat, more certain than ever that I might never feel the warmth of those shoes on my feet again. Daryl finished strapping me in, and I grabbed onto my two tiny little bicycle handlebar grips. "Oh yeah, like this is safe," I thought to myself. 

Tammy looked over at me as we were being cranked into the air. tink tink tink tink tink tink tink tink tink tink tink tink tink tink... the bungee cables stretched up to the tops of the bars. "I never can tell when this thing is going to take off," she said. tink tink tink tink tink tink tink tink tink tink tink tink click... "Oh," I gulped. "Right about nooooooooooooooowww," I yelped, as we shot into the sky.

Now, I must say, the initial fling into the air and the subsequent 300 foot drop, really wasn't that bad. It was very similar to a rollercoaster ride. Of course, I was still holding on for my dear life, and I really didn't like the feeling of lifting out of my seat as we began to plummet back to earth. But, this wasn't the worst part. That came next. 

As we began to bounce back into the air, the frickin ball began to rotate! So, now I'm not sitting upright, 300 feet in the air. No, I'm facing the fucking ground, and falling to my death! Again, my body is separating from the seat, and with my fear of falling, this was not a good situation to be in. 

I began torquing myself into the seat. I was pushing so hard that the little handlebar grips began sliding off. So here I am, falling to my certain death, and POP, my hand flies free in front of me, and I begin separating from my seat again. I let it go and grab the bar just in time for my other hand to come free. Obviously, I'm freaking the fuck out (I still have no idea how I managed to keep my mouth shut and smile this whole time). I dropped the other bright red grip and latched onto the bars with every ounce of strength I had. If I was gonna to die, then this bitch was going with me. I was not coming out of this seat, no matter how lose the harness was!

Mind you, these falls took place over a couple seconds. I never realized how fast we think or experience life in those final terrifying moments before we die.

This bouncing went on and on; upside down, backwards, slantways, you name it. The whole time, Tammy was laughing, ha ha ha,  ha ha ha,  ha ha ha,  and telling me how much she loved this ride. All I could think about was how I wanted to put my hands around her throat and squeeze. I just couldn't understand how someone could enjoy something so much. There's obviously a level of trust or recklessness there that I just can't comprehend.

Finally, we started settling right side up as the ball stopped bouncing. Daryl lowered us back to the ground and began unstrapping Tammy.  She was as giddy as a school girl. She was high on adrenaline and loving life. After Daryl unstrapped me, we discovered that I couldn't get out of the seat. I had held on so tightly that I locked all of my joints. I couldn't let go. Daryl began prying my hands off of the bars and helping me straightening them. Right about that time, he noticed that his little red handlebar grips were missing. "Dude, where are my grips?" he inquired. I turned, with absolute contempt in my voice and a sneer on my face, I responded "They're probably laying in the fucking highway!" As his jaw dropped at my response, I slipped on my shoes, and we left. 

In the end, I'm glad that I had the experience. I'll never do it again, but it was one hell of a rush! And, it all came about because I kept my word, in the face of one of my biggest fears. I feel as though I overcame a very large obsticle that day, and I'm proud of the courage that I was able to muster up when I needed to. I also learned that from now on, I'm going to find out what I might agree to before I give my word. ;)


