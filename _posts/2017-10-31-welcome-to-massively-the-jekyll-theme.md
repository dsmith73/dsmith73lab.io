---
layout: post
title:  "Applebee's"
date:   2005-11-12
excerpt: ""
image: "/images/crying.jpg"
---

Shortly after my divorce, I met a girl that wasn't really my type. Having had failed experiences with women which fit into this stereotypical category of "type," I decided to go outside the norm and give it a shot. She chose Applebee's over on Riverside. 

We arrived, and to my dismay, the place was absolutely packed. We ended up waiting about 10 minutes for a table. After we were seated, we ordered a couple drinks and began talking. We were geting along extremely well! She ordered a chicken dinner and I ordered a steak; medium. We continued to talk, and when the food arrived, in what only seemed to be a few moments, we dug-in. 

It was an outstanding meal, except my steak was still trying to walk off the plate (extremely rare). 

In general, when I go to a restaurant and they screw up the meal, I just don't go back again. However, if I've been going somewhere for a long time, and they screw up my meal, I tend to give them the benefit of the doubt. This was my first time at this establishment... Enough said...

Having decided that I wouldn't be returning, I ate the good portions of my steak, and the sides. Thankfully, the rest was quite tasty, and I was full. She finished up her meal and we began talking again. Quickly, she noticed that I hadn't finished my steak and asked me what was wrong with it. I told her how it wasn't cooked thuroughly enough for me and that I wasn't going to eat it, however, the rest of the meal was fantastic and I wasn't hungry.

She immediately began yelling for our waitress. When she arrived, my date (let's call her Rebecca) began chewing the waitress a new one. This poor girl was nearly in tears, and it certainly wasn't her fault that my meal was botched. Rebecca was creating such a scene that the manager took quick notice and came over to diffuse the situation. Obviously, he was extremely apologetic and insisted that we allow him a chance to replace our meals and accept free deserts. But that wasn't good enough. Rebecca tore into him until our meals were free, we had free deserts, a couple coupons, and my freshly remade meal with a desert to go. The whole time, I could only sit there in awe, looking at her, thinking "If you act like this on a first date, then do you really think there'll be a second?"

As the manager walked away, sore from the ripping of a second ass, Rebecca turned to me with a smug look on her face and said "Ha! You see that?! I showed him who was boss, and on top of that, I got our meals for free!" I was speechless. Moments later, my sympathy meal arrived. I scooped up my belongings and shuffled out the door as quickly as I could.

Thank God we met there! I walked her to her car, and she lingered, waiting for a kiss. I promptly handed her my extra meal and said "You should take this." She responded "It's your meal. Take it home and eat it for left-overs." To which I said "You deserve it more than I; our waitress' tears were a tribute to that. Now if you'll excuse me, I really must be going." She wasn't too pleased with that response, and she began barking for me to come back and give her a second chance. She said that she was only acting like that in the restaurant because she was nervous, and afraid that the screwed up meal might have affected our chances for a successful date. All I could say in return was "No, you accomplished that one all on your own."

A meal can be an extremely important part of any date. However, a screwed up meal won't destroy a date; creating a scene will... embarrassing your date will... attacking innocent people will. Try to take things in stride; accept the fact that there are imperfections in everything, and be thankful when things go your way. If the meal is really so bad that you need to complain, then pull the manager off to the side and discuss it with them. You'll still get what you want and you save them face at the same time.

