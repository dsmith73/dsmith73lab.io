---
layout: page
title: Dan Smith - dsmith73 - about
description: Come into my world.
sitemap:
    priority: 0.7
    lastmod: 2020-05-03
    changefreq: weekly
---
## About Dan

<span class="image left"><img src="{{ "/images/pic04.jpg" | absolute_url }}" alt="" /></span>

  I always feel like I'm learning, re-learning, trying to learn, or more importantly, *figuring something out*; and since people are always asking me how I do what I do... here's a bit of my backstory.  


### DigDug!
<div class="box">
  <p>
  I started playing with code in the late 70s / early 80s, on a Commodore 64 that my dad bought. He used to get tons of different science magazines, and in particular, Popular Science <i>(I think that was the one)</i> used to send out snippets of binary code in their magazines. Being somewhat unruly, I managed to get grounded for a couple of weeks. So, my friend and I discovered these snips; one such snip was DigDug. I spent so many quarters on that dang game, so when I saw it, I was like <i>"Whoa! Check this out! If we type these pages of 1s and 0s into the computer, we can play Dig-Dug for FREE!!</i>  
  </p> 
  <p>
  So, we went down to my room, and started typing... line after line... column after column... page after... <b>Wait!</b> Where's the rest of it!? <i>In another issue of the magazine.</i> Ugh!... We dug out another issue, and another, and this went on for a month or so, until we finally received the last magazine <i>with the last snipped of binary</i>. We saved the <code>.bin</code> file on our lovely 5 1/4" floppy <i>(double-density, thanks to my Mom's hole-punch)</i>, and typed in the magical <code>run</code> command. The command prompt disappeared, and the colorful arcade-<i>ish</i> DigDug screen appeared. We celebrated for a second, and then started playing.  
  </p>
  <p>
  Unfortunately, we'd missed a 1 or a 0 <i>(or several)</i> somewhere, because the hose would blotch out from time to time, and the exploding creatures were more like a pixelated blocks than the KA-BOOM that we expected. But, the game worked! We played that thing for hours...  
  </p>
  <p>
  Since that day, I've always been amazed by code, how it can translate those <code>words</code> or <code>dwords</code> into actions, and how those actions can improve our lives.  
  </p>
</div>

<span class="image left"><img src="{{ "/images/pic05.jpg" | absolute_url }}" alt="" /></span>

  **From there**, I moved on to batch files, video-games on my Dad's Amiga *(shout out to `The Bard's Tale`)*, and hours of figuring out Windows 1.1, etc... etc... down in the basement, in the last house on the left.

  Needless to say, I grew up on Windows, and never really had much exposure to Linus' invention - Linux. 

  I joined the military straight out of highschool, traveled the world, and lived the next 8-ish years outside the continental US. 

  I remember discovering <em>The Internet</em> for the first time while I was living in Southern Spain. Back then, people weren't thinking about security or how you could just simply browse into the computers of your neighbors - everything was wide-open, for better or worse. This re-kindled my interest, and knowing that I had no intention of spending another fifteen years in the military, I enrolled in college, and started taking computer courses. C++ 3.0 programming, databases, etc...


#### Fast-Forward to Today   

  *I'm always learning* - 
Linux, Ansible, Terraform, Docker, Kubernetes, Javascript, Markdown, Python, SQL, Splunk, AppDynamics (ADQL), Prometheus...; and the list goes on and on...  
  I'm not the king of any, but I am tenacious, and I rarely give up.  

  So, try not to judge the *(more than likely)* simple or already known answers that I post to issues, and the amount of simplicity, or perhaps complexity, of my setup.  
  If you know of a better way, then <em>Please</em> drop me a line and let me know! 


