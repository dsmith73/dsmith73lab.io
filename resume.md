---
layout: page
title: Dan Smith - dsmith73 - resume
description: This is what I've been up to in recent years.
sitemap:
    priority: 0.7
    lastmod: 2020-05-03
    changefreq: monthly
---

### **Executive Summary:**
<div class="box">
  <p>
  Technology leader with over twenty years of global, diverse industry experience, focusing on: improvement, analytics, strategy, customer experience, management, change, and business continuity. I achieve success by disrupting and breaking down siloed mentality and driving collaborative, accelerated solutions, to improve effectiveness. I am a visionary and a team player who leverages probability and causality analyses to identify opportunities and drive decisions. I inspire others to join me on my path for continuous improvement. I focus on the goal that we desire to achieve, how it translates to the business plan, and the benefits to be realized.
  </p>
</div>

#### **Areas of Excellence:** 
<table style="float:center">
  <tr>
    <td> Leadership </td>
    <td> Influencing </td>
    <td> Improvement </td>
    <td> Team Building </td>
    <td> Analytics </td>
  </tr>
  <tr>
    <td> Strategy </td>
    <td> Communication </td>
    <td> Problem Solving </td>
    <td> Automation </td>
    <td> ITIL </td>
  </tr>
  <tr>
    <td> Relationships </td>
    <td> Decision Making </td>
    <td> Customer Focus </td>
    <td> Goal Setting </td>
    <td> Change </td>
  </tr>
</table>

<hr>

### **Professional Experience:**

##### **2019 February – Present – KAR Global: Senior Manager – Enterprise Operations –**
  <p>
  Responsible for 24x7x365 monitoring and operational response at KAR Global; to include hiring, development, retention of North American and offshore teams; and the configuration and management of systems used to monitor internal and external customer applications and revenue generating architecture to deliver maximum uptime and revenue.  
  </p>
  - Migrated Event Management out of ServiceNOW, saving ~$600k per year by removing the ITOM module.
  - Reduced cost in 2019 by ~$200K and simplified architecture through consolidation of monitoring platforms.
  - Solidified Automation-First mindset within the NOC created a development specific team to deliver solutions.
  - Delivered framework by which monitoring of systems could be automated in the development pipeline – placing the configuration in the hands of the subject matter experts, and increasing the accuracy of monitoring.
  - Built automation for process documentation of code to increase the reliability of runbook creation for teams.
  - Developed and deployed standardized environment for idempotent automation build.
  - Worked with vendors to deliver standard frameworks for monitoring, metrics capture, and API integration.
  - Automated security audits for the NOC and multiple IT teams to increase compliance.
  - Partnered with Information Risk to improve monitoring and audit coverage of infrastructure, removing several gaps.
  - Successfully configured and implemented PagerDuty, migrating on-call rotation for IT teams, and reducing mean-time to act (MTTA) from ~15 minutes to less than 2 minutes.
    - Developed Terraform and Ansible plays to deploy and configure PagerDuty, reducing configuration time from 2 months to 8 hours and re-deploy time from 1 week to 30 seconds.
  - Implemented Splunk in the NOC and created several views which vastly increased observability of monitored systems and logged events, enabling application teams to isolate and resolve several key environmental issues.

##### **2015 July – 2019 February – NetEnrich: Senior IT Operations Manager –**  
  <p>
  Leadership team member overseeing 37 multi-cultural leads and associates, across varying skillsets in Enterprise Operations Center (ECC / EOC / NOC) and Extended Team / MSO engagements for IaaS, SaaS, &amp; PaaS, at KAR Auction Services &amp; North Central Region clients, on-site and remote, to exceed business goals, SLA, OLA, and KPI targets. Lead continuous integration / continuous deployment (CICD), change, and operations initiatives to exceed 24x7x365 global operations and 99.5% availability. Engage with development teams to develop / deploy competitive solutions to SaaS platforms. Manage business relationship and influence engagement growth year on year. Responsible for Contract, OLA, SLA, and SOW, Vision, Analytics, Processes, and Implementation. Responsible for performance, team development, issue resolution, and member management.  
  </p>
  - Re-architected monitoring to focus on Service Availability and Health to maximize revenue.
    - Led engagement initiatives to deliver SolarWinds, OpsRamp, Splunk, and PagerDuty.
  - Team delivered additional 38% alert noise reduction in Q1 of 2018 through analysis of Alert recovery time.
    - On-demand health reports to proactively identify intermittent infrastructure issues and opportunities for threshold adjustment.
    - Process to Alert association – 30% improvement across 22000 alerts.
    - Identification and Analysis of trends – Identified 6 previously unknown capacity issues.
    - Proactive identification – Addressed 14 defective core devices and 3 metric adjustments.
  - Led change initiatives reducing Alert/Event noise >50%, Incident noise >20%, false positives >15% in 2017.
    - Delivered constraint based analysis and predictive analysis models to service and technology teams.
  - Implemented cadence on tower and service analysis to identify continuous opportunities for improved performance
  - Audit initiatives reduced 15 flawed process areas from >30% failure rate to less than 1%.
  - Engaged by SaaS partner for micro service development methodology to help them improve their existing platform.
    - Consulted on health scoring for Alert, Incident, Change, Problem, Dependency, and Connection for tie-in to predictive models and criticality automation- adopted by ServiceNow &amp; OpsRamp for Probability models.
  - Delivered predictive analysis, initially providing 80% accuracy on outages and 95% accuracy on hardware failures.
  - Surpassed regional growth targets year on year and increased business engagements by 70% from 2015 to 2017.
  - Deployed process and priority standardization, MTTR, MTBF, and KPI based workload analyses, enabling reduction of SLA metrics, elimination of backlog, and better reporting through ServiceNow performance analytics.  
    - Improved service 19% in 2015, additional 23% in 2016, First to Know (FTK) by 18%, resolve rate 10% with Single Pane of Glass (SPoG), ITSM changes, ITIL improvements, and >300 knowledge articles.  
  - Lowered total cost of ownership (TCO) by 3%, and improved SLTs through re-negotiation and service transition.
  - Improved document management and audit processes, reducing error rate by more than 60%.
  - Developed probability analyses of workload and services to enable transition to bi-modal IT operations, accurately forecast resource levels, and drive continued efficiency improvements with Incident, Request, and Change.  

##### **2007 June – 2015 July – Rolls-Royce: Regional IT Strategy and Delivery Manager**  **–**  
  <p>
  Manage and facilitate business improvement, IT project delivery in North America, South America, and Canada. Focal point for change, delivery issues and environment upgrades. Board approver for changes to business environment. Chair of Engineering IT engagement committee. Member of IT Executive group. Technical approver for Engineering IT renewals in US. Charged to develop Engineering IT Strategy and high level roadmap annually.  
  </p>
  - Delivery of 64-bit OS project improved compute capability 80% in analytical community >2.4M yearly cost savings.  
    - Roadmap adopted globally for mobile device implementation.  
  - Improvements to management of 3.2M budget reduced spend by 1.2M for 43% efficiency improvement.  
  - Mitigated \&gt;$200M in cost by auditing sites being sold from Rolls-Royce to Siemens.  
  - Oversaw project recovery and facilitation of deliverables – Recovered 4 month slip in outsourcing initiative.  
  - Changes to agile environment increased engineering efficiency by $900K per year.  
  - Deployed timekeeping on mobile devices to improve accountability and time tracking for CAPEX deliveries.  
  - Delivered Six Sigma project to improve renewal processes and budgeting accuracy.  
  - Implemented deployed license tracking and auditing, recovering >$800K in license cost.  
  - Consulted on decommissioning of failing document management system and migration into global database, minimizing risk and standardizing record keeping across region.  
  - Kaizen to improve management of compute resources reduced yearly cost by $14M annually.  
  - Increased efficiency of Ceramic Matrix Composite (CMC) lab; resulting in >30% improved performance of graphical and analytical capability, enabling vastly increased global competiveness.  
  - Refactored existing &quot;all-inclusive&quot; bespoke applications to leverage common modules for streamlined code delivery.  
    - Successful delivery of 27 .NET applications to customers with new modular (microservice) design.  
    - Implemented CI/CD with new architecture, allowing for near 100% availability of applications while deploying patches, and minimizing production &quot;touches&quot; to deliver capability across multi-tenant platforms.  
  - Modular design (micro service) initiatives enabled 40% delivery improvement and reduced rework of bespoke code.  
    - Successful delivery of 27 .NET applications to customers with new modular (microservice) design.  
    - Implemented CI/CD with new architecture, allowing for near 100% availability of applications while deploying patches, and minimizing production &quot;touches&quot; to deliver capability across multi-tenant platforms.  
  - Led Trent 1000 CX war room design, allowing effective project progression toward milestones until strategic delivery, facilitating the move of the Trent CX work package to Indianapolis and securing 120 Full Time Equivalents (FTEs) of work to offset F136 program shutdown.
  - Modular design (micro service) initiatives enabled 40% delivery improvement and reduced rework of bespoke code.  
  - Leveraged by SAP group for ERP security and workflow enhancements to improve data integrity.  
  - Led large change initiative to upgrade 2400 engineers, and delivered three months ahead of schedule.  
  - Chair of Engineering IT Working Group, charged with employing innovative technologies to overcome competition.  
  - Invited to join the North American IT Executive Group and Gas Turbine Supply Chain (GTSC) IT Strategy Board, representing Engineering &amp; Technology interests for North America.  

---

### **Education, Certifications, and Training:**

<table>
  <tr>
    <th> Year </th>
    <th> Description </th>
  </tr>
  <tr>
    <td> 2019 </td>
    <td> AppDynamics certification through 300 </td>
  </tr>
  <tr>
    <td> 2019 </td>
    <td> Splunk Certification </td>
  </tr>
  <tr>
    <td> 2015 </td>
    <td> Rolls-Royce Six Sigma Green Belt, Indianapolis, IN </td>
  </tr>
  <tr>
    <td> 2015 </td>
    <td> Control Account Management &amp; Earned Value Management, Indianapolis, IN </td>
  </tr>
  <tr>
    <td> 2012 </td>
    <td> Rolls-Royce Six Sigma Yellow Belt, Indianapolis, IN </td>
  </tr>
  </tr>
  <tr>
    <td> 2009 </td>
    <td> Certified International System Security Professional (CISSP) Training, Indianapolis, IN </td>
  </tr>
  <tr>
    <td> 2008 </td>
    <td> Teamcenter Administration Certification, Siemens, St Louis, MO </td>
  </tr>
  </tr>
  <tr>
    <td> 2002 </td>
    <td> Anti-Terrorism Training, Kaiserslautern, Germany </td>
  </tr>
  <tr>
    <td> 1998 </td>
    <td> Management Information Systems, University of Maryland (Europe), Rota, Spain </td>
  </tr>
  <tr>
    <td> 1996 </td>
    <td> Avionics Systems Technology – Community College of the Air Force – USAF </td>
  </tr>
  <tr>
    <td> 1995 </td>
    <td> Total Quality Leadership and Management, Rota, Spain </td>
  </tr>
  </tr>
  <tr>
    <td> 1995 </td>
    <td> Military Leadership Certification, Aviano, Italy </td>
  </tr>
  <tr>
    <td> 1992 </td>
    <td> Computer Security, Operations Security, Communications Security Certifications, Anchorage, AK </td>
  </tr>
  <tr>
    <td> 1991 </td>
    <td> High School Diploma – Thomas Jefferson High School – Rockford, IL </td>
  </tr>
</table>
